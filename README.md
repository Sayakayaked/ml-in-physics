# ML-in-physics

Introduction to machine learning methods in physics from classical ML to quantum machine learning.

## Description

The repository contains laboratory works of gradually increasing difficulty covering areas of machine learning (ML) relevant to physical problems
and new approaches to ML based on new physical principles.
Every laboratory work consists of motivational part describing real-world problem, outline of approaches to solve the problem
and tasks for students.
Code examples are provided in Python and introduce the reader to the software and libraries available in the field.

<!-- ## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method. -->
## List of laboratory works

1. [Intro: regression and classification](labs/intro.ipynb)

## References 

See [References page](REFERENCES.md).

## Installation

[Jupyter Notebook](https://jupyter.org/) or Jupyter Lab is required.
Version of Python newer than 3.9 is assumed to be installed.
Necessary libraries can be installed directly from labs using [pip](https://pip.pypa.io/en/latest/),
or using your favorite package manager (e.g. [Anaconda](https://www.anaconda.com/) is convenient for Windows users). 
Some labs do intensive computations, hence installed GPU and configured [CUDA](https://developer.nvidia.com/cuda-downloads) are highly desirable.

Your can avoid installation 

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

<!-- ## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc. -->

## Roadmap

1. Cover variational problems in quantum chemistry.
1. Cover quantum machine learning.

## Authors and acknowledgment

List of developers

* Igor Lobanov [lobanov.igor@gmail.com](mailto:lobanov.igor@gmail.com) (mantainer). 

Other contriubutors are welcome!
## Contributing

You can contribute by:

* solving the tasks, testing new laboratory works and reporting your experience,
* proofreading, improving english,
* sending links to new research on ML in physics,
* suggesting new problems and students tasks,
* fixing bugs, appending comments explaining complex parts of the code, improving readability,
* drawing attention to the new software and proving solution of existing tasks by the new means,
* any help is appreciated!

To introduce your modification directly to the source, you can 

1. Clone the repository.
2. Commit and push your update.
3. Request merge, see [details](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html). 

## License

The content of the course is free for non-commercial use.  

