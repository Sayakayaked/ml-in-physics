# Код ниже использует библиотеки numpy, numba, matplotlib, ipycanvas.
# Сами библиотеки можно установить с помощью pip следующей командой (запускать из терминала)
#    pip install numpy numba matplotlib ipywidgets 
# или аналогичной командой для conda.

# ipycanvas стиваться следующей командой:
#    pip install ipycanvas orjson 
# Если вы используете jupyter-lab, то потребуется еще одно команда:
#    jupyter labextension install @jupyter-widgets/jupyterlab-manager ipycanvas


import pickle
import time
from dataclasses import dataclass

import numpy as np
import numba as nb
import matplotlib.pyplot as plt

import ipywidgets as widgets
from ipycanvas import Canvas, hold_canvas

#######################################################################################
# Реализация игры по мотивам крестиков-ноликов.

def decode_move(m):
    assert len(m)==2
    r, c = ord(m[0]), ord(m[1])
    c -= ord('0')
    if r>=ord('a'):
        r -= ord('a')
    else:
        r -= ord('A')        
    return r, c

def encode_move(m):
    r, c = m
    return f"{chr(ord('A')+r)}{c}"

@nb.njit('i2(i8)')
def sumofbits(n):
    for i, mask in enumerate([0x55555555, 0x33333333, 0x0F0F0F0F, 0x00FF00FF, 0x0000FFFF]):
        n = (n & mask) + ((n >> (1 << i)) & mask)
    return n

@nb.njit('i2[:](i8[:])')
def sumofbits_map(x):
    N, = x.shape
    y = np.empty(N, np.int16)
    for k in range(N):
        y[k] = sumofbits(x[k])
    return y

assert np.all(sumofbits(0xffffffff)==32)

@nb.njit('i8(i8,i8)')
def shiftdown(x,SIZE):
    NBITS = SIZE*SIZE
    ALLBITS = (1<<NBITS) - 1
    y = (x<<SIZE)&ALLBITS
    return y

@nb.njit('i8[:](i8[:],i8)')
def shiftdown_map(x,SIZE):
    NBITS = SIZE*SIZE
    ALLBITS = (1<<NBITS) - 1
    N, = x.shape
    y = np.empty(N, dtype=np.int64)
    for k in range(N):
        y[k] = (x[k]<<SIZE)&ALLBITS
    return y

@nb.njit('i8(i8,i8)')
def shiftup(x,SIZE):
    y = x>>SIZE
    return y

@nb.njit('i8[:](i8[:],i8)')
def shiftup_map(x,SIZE):
    N, = x.shape
    y = np.empty(N, dtype=np.int64)
    for k in range(N):
        y[k] = x[k]>>SIZE
    return y

@nb.njit('i8(i8,i8)')
def shiftright(x,SIZE):
    mask = 1
    for _k in range(SIZE):
        mask |= mask<<SIZE
    mask = ~mask
    y = (x<<1)&mask
    return y

@nb.njit('i8[:](i8[:],i8)')
def shiftright_map(x,SIZE):
    mask = 1
    for _k in range(SIZE):
        mask |= mask<<SIZE
    mask = ~mask
    N, = x.shape
    y = np.empty(N, dtype=np.int64)
    for k in range(N):
        y[k] = (x[k]<<1)&mask
    return y

@nb.njit('i8[:](i8[:],i8,i8)')
def linescount_map(x,SIZE,LENGTH):
    N, = x.shape
    y = np.empty(N, np.int64)
    for k in range(N):
        m1 = m2 = m3 = m4 = x[k]
        for _l in range(LENGTH-1):
            m1 &= shiftdown(m1, SIZE)
            m2 &= shiftright(m2, SIZE)
            m3 &= shiftdown(shiftright(m3, SIZE), SIZE)
            m4 &= shiftup(shiftright(m4, SIZE), SIZE)
        y[k] = sumofbits(m1)+sumofbits(m2)+sumofbits(m3)+sumofbits(m4)
    return y

@nb.njit('b1(i8[:],i8)')
def isany_map(x,SIZE):
    N, = x.shape
    y = np.int64(0)
    for k in range(N):
        y |= x[k]
    allones = (1<<(SIZE*SIZE))-1
    return y^allones==0

@nb.njit('i8(i8[:])')
def or_reduce(x):
    N, = x.shape
    y = np.int64(0)
    for k in range(N):
        y |= x[k]
    return y

@nb.njit('i2[:](i8[:])')
def intersectioncount(marks):
    N, = marks.shape
    count = np.empty(N, dtype=nb.int16)
    for n in range(N):
        s = np.int64(0) 
        for k in range(N):
            if k!=n: s |= marks[k]
        count[n] = sumofbits(s&marks[n])
    return count


# class IllegalMove(Exception):
#     pass

SIZE = 4 # Размер доски SIZE x SIZE.
NPLAYERS = 2 # Число игроков.
LINELENGTH = 4 # Длина линии для выигрыша.

spec = [
    ('_marks', nb.int64[:]),
]

@nb.experimental.jitclass(spec)
class Board:
    """Объект, хранящий состояние игры в крестики-нолики."""

    def __init__(self, marks):
        """
        Создает поле игры, отметки каждого игрока хранятся в аргументах vargs.
        Крестики соответствуют vargs[0], нолики vargs[1].
        Позиции задаются битовой маской, где 1 находится в позициях, в которых стоит отметка.
        Поле в стороке r и столбце соответствует биту с номером r*SIZE+c.
        """
        # assert isinstance(marks, np.ndarray) and marks.dtype==np.int16
        # assert marks.shape[0]==NPLAYERS        
        self._marks = marks.copy()
        
    # @property
    # def astuple(self):
    #     return tuple(self._marks)
                
    @property
    def current_player(self):
        """
        Возвращает номер игрока (начиная с нуля), который сейчас делает ход.
        """
        bs = sumofbits_map(self._marks)
        o = None
        for i, m in enumerate(bs):
            n = bs[i]
            if o is not None and o>n:
                return i
            o = n
        return 0
        
    def toindex(self,r,c):
        """Возвращает индекс бита, хранящего элемент в строке r и столбце c."""
        # assert 0<=r<SIZE and 0<=c<SIZE
        return r*SIZE+c
    
    def __getitem__(self, key):
        """Возвращает список игроков, поставивших отметки в указанной позиции."""
        r, c = key
        mask = 1 << self.toindex(r, c)
        N, = self._marks.shape
        b = np.zeros((N,), dtype=np.int8)
        for i, m in enumerate(self._marks): 
            b[i]=1 if m&mask else 0
        return b
    
    def make_move(self, key, value):
        """Устанавливает отметку игрока value в позиции key."""
        if value!=self.current_player:
            return None
        r, c = key
        # assert isinstance(r, int) and isinstance(c, int)
        mask = 1 << self.toindex(r, c)
        # assert 0<=value<NPLAYERS
        # if self._marks[value]&mask: # Если в клетке есть своя отметка.    
        if or_reduce(self._marks)&mask: # Если клетка не пуста.
            return None
        marks = self._marks.copy()
        marks[value] |= mask
        return Board(marks)
        
    @property
    def score(self):
        """
        Возвращает массив выигрышей для каждого из игроков для терминального состояния игры.
        Возвращает None, если еще доступны ходы.
        """
        
        # Ищем отметки разных игроков в одной клетке.
        score = -intersectioncount(self._marks)
        if sum(score)<0: return 5*score
        
        # Ищем пересечения.
        score = linescount_map(self._marks,SIZE,LINELENGTH) 
        if sum(score)>0: return 2*score-1
                
        # Проверяем, есть ли свободные поля.
        if isany_map(self._marks,SIZE): 
            return np.zeros(NPLAYERS, dtype=np.int64) # Если нет, заканчиваем игру с нулевым счетом.

        return None # np.zeros((0,), dtype=np.int64)
    
    def all_moves(self):
        player = self.current_player
        moves = []
        for r in range(SIZE):
            for c in range(SIZE):
                # if player not in self[r,c]: # Можно ходить, если в клетке нет своего знака.
                if not np.any(self[r,c]): # Можно ходить, если клетка пуста.
                    moves.append((r,c))
        return moves
    
    def __eq__(self, other):
        return isinstance(other, Board) and np.all(self._marks==other._marks)
    
    def __hash__(self):
        return np.sum(self.astuple)
    
    def shiftup(self):
        return Board(shiftup_map(self._marks, SIZE))
    
    def shiftdown(self):
        return Board(shiftdown_map(self._marks, SIZE))
    
    def shiftright(self):
        return Board(shiftright_map(self._marks, SIZE))

    def intersection_count(self, other):
        return sumofbits_map(self._marks & other._marks)
    
def initial_board():
    return Board(np.zeros(NPLAYERS,dtype=np.int64))

#########################################################################

def cross_heuristics(board):
    b1 = b2 = b3 = b4 = board
    heuristics = np.zeros(NPLAYERS)   
    for n in range(1, LINELENGTH):
        b1 = b1.shiftup()
        heuristics += board.intersection_count(b1)

        b2 = b2.shiftright()
        heuristics += board.intersection_count(b2)

        b3 = b3.shiftright().shiftup()
        heuristics += board.intersection_count(b3)

        b4 = b4.shiftright().shiftdown()
        heuristics += board.intersection_count(b4)
        
    return heuristics

def repr_board(board, MARKS = ('⤫','◯','⬦')):
    lines = []
    lines.append(f" {''.join(f'{c}' for c in range(SIZE))}")        
    for r in range(SIZE):
        chars = [f"{chr(r+ord('A'))}"]
        for c in range(SIZE):
            p = board[r,c]
            n = np.sum(p)
            chars.append( '⋅' if n==0 else ('!' if n>1 else MARKS[np.nonzero(p)[0][0]]) )
        lines.append( "".join(chars) )
    lines[0] += f" Score {board.score}"
    lines[1] += f" Player {MARKS[board.current_player]}"
    lines[2] += f" Heuristics {cross_heuristics(board)}"
    return "\n".join(lines)


#########################################################################################
# Боты / Искусственный интеллект 

# Нашей основной задачей далее будет написание алгоритма, реализующего оптимальную стратегию для описанной выше игры.
# Здесь мы определим базовый класс `Player`, от которого будут наследовать все наши реализации.
# В качестве примера приведем пример тривиального бота `RandomPlayer`, делающего случайные ходы.

class Player:
    pass

class RandomPlayer(Player):
    def __init__(self):
        pass
    
    def __call__(self, board: Board):
        moves = board.all_moves()
        idx = np.random.randint(len(moves))
        return moves[idx]

################################################################################
# Графическое представление доски

def draw_mark(self, canvas, mark, x,y, s, border=0):
    canvas.line_width = 7
    if mark==0:
        canvas.stroke_style = 'blue'
        canvas.stroke_line(x+border, y+border, x+s-border, y+s-border)
        canvas.stroke_line(x+border, y+s-border, x+s-border, y+border)            
    elif mark==1:
        canvas.stroke_style = 'red'                
        canvas.stroke_circle(x+s/2, y+s/2, s/2-border)  
    elif mark==2:
        canvas.stroke_style = 'green'
        canvas.stroke_lines([
            (x+s/2, y+border), 
            (x+s-border, y+s/2), 
            (x+s/2, y+s-border),
            (x+border, y+s/2), 
            (x+s/2, y+border)
        ])
    else: raise NotImplementedError(f"Unimplemented mark {mark}.")

def draw_board(board:Board, canvas=None, width=1000):
    if canvas is None:
        canvas = Canvas(width=width, height=width+20, layout=dict(width="25%"))
    else:
        canvas.clear()
        width = canvas.width
    cellwidth=width/SIZE            
    with hold_canvas():
        canvas.stroke_style = "black"
        canvas.line_width = 1
        for n in range(1,SIZE):
            canvas.stroke_line(cellwidth*n, 0, cellwidth*n, cellwidth*SIZE)
            canvas.stroke_line(0, cellwidth*n, cellwidth*SIZE, cellwidth*n)
        for r in range(SIZE):
            for c in range(SIZE):
                ms = board[r,c]
                n = np.sum(ms)
                if n<1: continue
                nn = int(np.ceil(np.sqrt(n)))
                markwidth = cellwidth/nn
                t = 0
                for k, b in enumerate(ms):
                    if b:
                        rr, cc = t // nn, t % nn
                        t += 1
                        draw_mark(board, canvas, k, c*cellwidth+cc*markwidth, r*cellwidth+rr*markwidth, markwidth, border=0.1*markwidth)
        canvas.font = "32px serif"
        canvas.fill_text(f"Score {board.score} Heuristics {cross_heuristics(board)}", 10, width+10)
    return canvas


def play_game(bots=[None,None], board:Board=None, verbose:bool=False, hint=None):
    """
    Запуск игры в интерактивной режиме.
    
    Аргументы:
        bots - список ботов. Длина должна совпадать с числом игроков. 
            Если какой-то элемент списка содержит None, этот ход делает человек.
        board - начальное состояние доски.
        verbose - нужно ли выводить все ходы.
    """
    if board is None: # Инициализируем доску, если она не передана параметром.
        board = initial_board()

    global global_board
    global_board = board
        
    # Создаем место для текстового вывода.
    out = widgets.Output()
    
    def postoidx(x):
        """Вычисляет номер строки/столбца по координатам мыши."""
        return int(x/canvas.width*SIZE)

    def make_move(r,c):
        """Делает ход с проверкой корректности и выводом сообщений."""
        global global_board
        if global_board.score is not None:
            if verbose: print(f"End of game")
            return
        msg = f"Player {global_board.current_player}: move {r} {c}."
        board = global_board.make_move((r, c), global_board.current_player)
        if board is None:
            print(f"{msg} Illegal move: {r}, {c}")
        else:
            global_board = board
            if verbose:
                print(f"{msg} Score {global_board.score}")
    
    def moveit():
        """Делает ходы за ботов, пока это возможно."""
        global global_board
        while global_board.score is None:
            player = bots[global_board.current_player]
            if player is None: break
            move = player(global_board)
            make_move(*move)

    # Делаем ходы за ботов.
    moveit()
    
    # Инициализируем холст и отрисовываем доску.
    canvas = draw_board(global_board)
    display(canvas)
    # Выводим подсказку, если она есть.
    if hint is not None: hint(global_board)    
    
    # При нажатии кнопок мыши делаем ходы. 
    @out.capture()
    def handle_mouse_down(x, y):
        global global_board
        # Определяем координаты поля и делаем ход.
        c, r = postoidx(x), postoidx(y)
        make_move(r, c)
        # Делаем ходы за ботов.
        moveit()
        # Обновляем визуализацию доски.
        draw_board(global_board, canvas=canvas)
        # Выводим подсказку, если она есть.
        if hint is not None: hint(global_board)

    # Запускаем обработку сообщений.
    canvas.on_mouse_down(handle_mouse_down)
    display(out)

###################################################################################
# Решение игры / полный перебор дерева игры

class Cache:
    pass

class ArrayCache(Cache):    
    def __init__(self, nplayers, size):
        self._nplayers = nplayers
        self._size = size
        n = 2**(self._size**2)
        print(f"Elements in cache {n**self._nplayers}")
        self._cache = np.empty((n,)*self._nplayers+(self._nplayers,), dtype=np.float32)
        self._cache[:] = np.nan
        
    @property
    def size(self):
        return np.count(~np.isnan(self._cache[:,0]))
        
    @classmethod
    def from_board(cls, board:Board):
        return ArrayCache(nplayers=NPLAYERS, size=SIZE)
    
    def check_board(self, board):
        assert isinstance(board, Board)
        assert NPLAYERS==self._nplayers
        assert SIZE==self._size        
    
    def __setitem__(self, key, value):
        self.check_board(key)
        assert isinstance(value, np.ndarray)
        self._cache[key.astuple] = value
    
    def __getitem__(self, key):
        self.check_board(key)
        value = self._cache[key.astuple]
        return None if np.isnan(value[0]) else value
    
    def save(self, filename):
        np.savez_compressed(filename, states=self._cache)
    
    @classmethod
    def load(cls, filename):
        data = np.load(filename)
        cache = data['states']
        assert isinstance(cache, np.ndarray)
        nplayers = cache.shape[-1]
        assert cache.ndim-1==nplayers
        size = int(np.sqrt(np.log2(cache.shape[0])))
        assert cache.shape[:-1] == (2**(size**2),)*nplayers
        result = ArrayCache(nplayers=nplayers, size=size)
        result._cache[:] = cache
        return result

class DictCache(Cache):
    def __init__(self):
        self._cache = {}
            
    def __setitem__(self, key, value):
        assert isinstance(key, Board)
        assert isinstance(value, np.ndarray)
        self._cache[key.astuple] = value
    
    def __getitem__(self, key):
        assert isinstance(key, Board)
        return self._cache.get(key.astuple)
    
    @property
    def size(self):
        return len(self._cache)
    
    def save(self, filename):
        with open(filename, 'wb') as handle:
            pickle.dump(self._cache, handle, protocol=pickle.HIGHEST_PROTOCOL)

    @classmethod
    def load(cls, filename):
        with open(filename, 'rb') as handle:
            cache = pickle.load(handle)
        result = DictCache()
        result._cache.update(cache)
        return result      

def solve_game(board:Board, cache:Cache) -> np.ndarray:
    """
    Для данного начального состояния `board` составляет дерево игры и возвращает 
    """
    if cache is None:
        cache = ArrayCache.from_board(board)
    assert isinstance(board, Board)
    assert isinstance(cache, Cache)
    
    # Если состояние уже закешировано, возвращаем для него очки.
    score = cache[board]
    if score is not None: return score
    
    # Если состояние терминальное, то добавляем его в кеш и возвращаем очки.
    score = board.score
    if score is not None:
        cache[board] = score
        return score
    
    # Считаем потомков и находим оптимальный ход.
    player = board.current_player
    moves = tuple(board.all_moves())
    scores = []
    for idx, move in enumerate(moves):
        child = board.make_move(move, player)
        score = cache[child]
        if score is None:
            score = solve_game(board=child, cache=cache)
        scores.append(score)
        
    scores = np.array(scores)
    best_move = np.argmax(scores[:,player])
    score = scores[best_move]
    cache[board] = score
    
    return score
    
    
def find_best_move(board:Board, cache:Cache, verbose=False):
    if board.score is not None: return None
    moves = tuple(board.all_moves())
    player = board.current_player
    scores = np.array(tuple(cache[board.make_move(move, player)] for move in moves))
    p = np.exp( 2*(scores[:,player]-np.max(scores[:,player])) )
    p /= np.sum(p)
    if verbose: 
        print(f"\nBest score {*best_score,}. Player {player}. Moves {*(moves[idx] for idx in indices),}")
        for move in moves:
            b = board.make_move(move, player)
            print(f"{move} -> {*cache[b],}")
    idx = np.random.choice(np.arange(len(moves)), p=p)
    return moves[idx]

class CachePlayer(Player):
    def __init__(self, cache, verbose=False):
        assert isinstance(cache, Cache)
        self._cache = cache 
        self._verbose = verbose
    def __call__(self, board):
        assert isinstance(board, Board)
        move = find_best_move(board, self._cache, verbose=self._verbose)
        assert move is not None
        return move


####################################################################################
# Метод Монте-Карло

# Для больших досок перебрать все дерево игры невозможно, 
# поэтому для выбора хода мы делаем частичный перебор ходов.

class MCResult:
    def __init__(self, moves, estimate):
        self.moves = moves
        self.estimate = np.array(estimate)

    def choose_move(self, slope=5, verbose=False):
        h = self.estimate-np.max(self.estimate)
        p = np.exp(slope*h)
        p /= np.sum(p)
        idx = np.random.choice(np.arange(len(h)), p=p)
        return self.moves[idx]

    def __repr__(self):
        idx = np.argsort(self.estimate)
        return " ".join(
            f"{encode_move(self.moves[i])}({self.estimate[i]})" for i in idx 
        )

@dataclass
class TimeInfo:
    nstates: int 
    ngames: int
    used_time: float

    def __add__(self, other):
        return TimeInfo(
            nstates=self.nstates+other.nstates, 
            ngames=self.ngames+other.ngames,
            used_time=np.maximum(self.used_time, other.used_time),
            )

    def __repr__(self):
        states_per_sec = self.nstates/self.used_time
        return f"Played {self.ngames} games, {self.nstates} states: {states_per_sec:.2f} states/sec."

class Node:
    def __init__(self, board: Board, minimum_score=-2):
        assert isinstance(board, Board)        
        self.board = board
        self.player = self.board.current_player
        self.score = self.board.score
        self.moves = self.board.all_moves()
        self._children = None
        
        self.children_heuristics = None
        self.nvisits = 0 
        self._children_coverage = None
        self.estimate = minimum_score*np.ones(NPLAYERS) if self.score is None else self.score

    def to_result(self):
        moves = tuple(self.moves)
        estimate = tuple(child.estimate[self.player] for child in self.children)
        return MCResult(moves=moves, estimate=estimate)

    @property
    def coverage(self):
        if self.score is not None: return 1.
        return np.mean(self.children_coverage)

    @property 
    def children_coverage(self):
        if self._children_coverage is None:
            self._children_coverage = np.zeros(len(self.children), dtype=np.float32)
        return self._children_coverage

    @property
    def children(self):
        if self._children is None:
            self._children = tuple(Node(self.board.make_move(move, self.player)) for move in self.moves)
        return self._children


class MonteCarlo:
    def __init__(self, board: Board, verbose=False):
        assert isinstance(board, Board)
        self.board = board
        self.root = Node(self.board)
        self.verbose = verbose
    
    def __repr__(self):
        node, children = self.root, self.root.children
        pmc = self.get_probabilities(node, children)
        lines = [repr_board(node.board)]
        # print(f"Player {node.player} Score {node.score}")
        for i, (move, child) in enumerate(zip(node.moves, children)):
            lines.append(f"{'*' if i==idx else ' '}{encode_move(move)}: P={p[i]:.3f} mc={pmc[i]:.3f} n={child.nvisits:3d} c={node.children_coverage[idx]:.5f} e={child.estimate} h={node.children_heuristics[i]:.2f}")
        return '\n'.join(lines)

    def run_games(self, allocated_time:float=3.):
        """
        Играет несколько случайных игр, строя часть дерева игры.
        Возвращает число рассмотренных состояний.
        """
        ngames, nstates = 0, 0
        start_time = time.process_time()
        while True:
            ngames += 1
            current_time = time.process_time()
            used_time = current_time - start_time
            if used_time>allocated_time: break
            nstates += self.run_move(self.root)
        info = TimeInfo(ngames=ngames, nstates=nstates, used_time=used_time)
        return info, self.root.to_result()
                    
    def run_move(self, node: Node):
        """
        Выбирает и делает один ход в игре, вызывается рекурсивно, 
        пока не достигнет терминального состояния.
        Возвращает число рассмотренных состояний/чило рекурсивных вызовов.
        """
        # Увеличиваем счетчик посещений узла.
        node.nvisits += 1
        # На терминальных состояниях игры останавливаемся.
        score = node.score
        if score is not None: 
            return 0
        
        # Перебираем все допустимые ходы.
        children = node.children
        # Вычисляем вероятности ходов.
        p = self.get_probabilities(node, children)
        # assert p.shape == (len(children),)
        # Делаем случайный ход.
        idx = np.random.choice(np.arange(len(children)), p=p)
        child = children[idx]
        # Переходим к следующему ходу.
        nstates = self.run_move(child)
        
        # Обновляем оценку выигрышей.
        self.update_estimate(node, child)

        # Обновляем покрытие.
        node.children_coverage[idx] = child.coverage
        
        return nstates+1
    
    def update_estimate(self, node, child):
        """
        Обновляет оценку выигрыша при оптимальной игре.
        """
        # assert isinstance(node, Node)
        # assert isinstance(child, Node)
        if child.estimate[node.player]>node.estimate[node.player]:
            node.estimate = child.estimate.copy()

    def get_probabilities(self, node, children, slope=1., shift=1.):
        """
        Вычисляет вероятности посещения дочерних узлов при обходе дерева.
        """
        # Сохраняем оценку выигрыша для текущего игрока.
        h = np.array(tuple( child.estimate[node.player] for child in children ), dtype=np.float32)
        h -= np.max(h)+shift # Сдвигаем очки, так что максимум равен -shift<0.
        # assert np.all(h<0)
        # Сохраняем процент покрытия ветвей.
        c = node.children_coverage
        # assert np.all(np.logical_and(0<=c, c<=1))
        # Корректируем предпочтительность ходов, уменьшая притягательность уже изученных вариантов.
        h /= 1.01-c
        # Находим вероятности переходов.
        p = np.exp(slope*h)
        # print(f"{h=} {p=}")
        p /= np.sum(p)
        return p

    
######################################################################################
    
class MCPlayer(Player):
    """
    Бот, использующий Монте-Карло для обхода дерева игры.
    """
    def __init__(self, verbose=False, ngames=1000):
        self._verbose = verbose
        self.ngames = ngames
    def __call__(self, board):
        assert isinstance(board, Board)
        mc = MonteCarlo(board, verbose=self._verbose)
        timeinfo, result = mc.run_games(ngames=self.ngames)
        if verbose:
            print(timeinfo)
            print(result)
        move = result.choose_move()
        return move


########################################################################################
# Эвристики

class HeuristicMonteCarlo(MonteCarlo):
    """Заготовка для обхода дерева с учетом эвристики."""
    def children_heuristics(self, node: Node):
        """Эвристика, оценивающая предпочтительность состояний игры."""
        if node.children_heuristics is None:
            node.children_heuristics = np.empty(len(node.children), dtype=np.float32)
            for idx, child in  enumerate(node.children):
                a = cross_heuristics(child.board)
                node.children_heuristics[idx] = np.sum(a[node.player]-a)
        return node.children_heuristics
        
    def get_probabilities(self, node, children, slope=1., shift=1.):
        # Вычисляем эвристики
        h = self.children_heuristics(node)
        h -= np.max(h)+shift # Сдвигаем очки, так что максимум равен -shift<0.
        # assert np.all(h<0)
        # Сохраняем процент покрытия ветвей.
        c = node.children_coverage
        # assert np.all(np.logical_and(0<=c, c<=1))
        # Корректируем предпочтительность ходов, уменьшая притягательность уже изученных вариантов.
        h /= 1.01-c
        # Находим вероятности переходов.
        p = np.exp(slope*h)
        # print(f"{h=} {p=}")
        p /= np.sum(p)
        return p

###########################################################################################

class HeuristicsPlayer(Player):
    """
    Бот, использующий Монте-Карло для обхода дерева игры.
    """
    def __init__(self, verbose=False, time=3):
        self._verbose = verbose
        self.time = time
    def __call__(self, board):
        assert isinstance(board, Board)
        mc = HeuristicMonteCarlo(board, verbose=self._verbose)
        timeinfo, result = mc.run_games(allocated_time=self.time)
        if self._verbose:
            print(timeinfo)
            print(result)
        move = result.choose_move()
        return move