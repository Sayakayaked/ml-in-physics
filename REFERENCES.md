# References on machine learning (ML)

## Wikipeda for the impatient

1. (Machine learning in physics)[https://en.wikipedia.org/wiki/Machine_learning_in_physics]
1. (Quantum machine learning)[https://en.wikipedia.org/wiki/Quantum_machine_learning]

## Classical ML

1. Ian Goodfellow and Yoshua Bengio and Aaron Courville. Deep Learning. MIT Press, 2016. [PDF is available](https://www.deeplearningbook.org/)

## Automatic differentiation

1. [autograd](https://github.com/HIPS/autograd)
1. [JAX: Autograd and XLA](https://github.com/google/jax)