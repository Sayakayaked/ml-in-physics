"""
Simple implementation of artificial neural networks for educational purposes.
Based on NumPy and is quite slow for real world applications.
"""

import numpy as np

############################################################################################################
# Loss functions
############################################################################################################

class Loss:
    """
    Loss function, that is a non-negative scalar-valued function.
    """
    def __init__(self, net):
        assert isinstance(net, ANN)
        self.net = net
        
    def __call__(self, x):
        """
        Return the loss function value `loss` on a vector `x` as well as gradient `grad_x` of the loss-function over `x`.
        """
        raise NotImplementedError
        return loss, grad_x
        
    def propagate(self, net, x):
        """
        Compute loss for application of `net` to input `x`, gradient `grad_x` of the loss over `x`
        and gradient `grad_net` of the loss over parameters of `net`.
        To update weigth of `net` the gradient `grad_net` should be averaged over samples (see `ANN.sum_samples`). 
        """
        x = np.asarray(x)
        if isinstance(net, Composition):
            y = net.b(x)
            loss, grad_y, grad_a = self.propagate(net.a, y)
            grad_x, grad_b = net.b.grad(x=x, y=y, grad_y=grad_y)
            return loss, grad_x, Composition(a=grad_a, b=grad_b)
        elif isinstance(net, ANN):
            y = net(x)
            loss, grad_y = self(y)
            grad_x, grad_net = net.grad(x=x, y=y, grad_y=grad_y)
            return loss, grad_x, grad_net
        else:
            raise TypeError
        

############################################################################################################

class MSE(Loss):
    def __init__(self, observed):
        self.observed = np.asarray(observed)
    
    def __call__(self, predicted):
        assert predicted.shape == self.observed.shape
        diff = predicted - self.observed
        n = predicted.size
        loss = np.sum(diff**2)/n
        grad_x = (2/n)*diff 
        return loss, grad_x
        

############################################################################################################
# Neurons
############################################################################################################

class ANN:
    """
    Artificial neural network as mapping y=f(x,p), where `x` is input, `y` is output, `p` is the parameter.
    Every object `net` of the class `ANN` stores parameters `p`.
    Given input `x` and the network `net`, the output `y` can be computed as `y=net(x)`.
    Given input `x`, the network `net`, the output `y=net(x;p)` and the gradient `dy` of a function `L(y)`
    over `y`, the gradients `dx` and `dp` of `L(net(x,p))` over `x` and `p` can be computed by 
    chain rule, implemented as `grad` method, can be used for backpropagation.
    """

    @property
    def indim(self):
        """
        Input vector dimensionality supported by stored parameters.
        Return `None` if output shape coindices with the input shape.

        """
        raise NotImplementedError 
    
    @property
    def outdim(self):
        """
        Output vector dimensionality supported by stored parameters.
        Return `None` if output shape coindices with the input shape.
        """
        raise NotImplementedError 

    def __repr__(self):
        return self.__class__.__name__
        
    def __call__(self, data):
        """Application of the network `self` to input vector `data`."""
        raise NotImplementedError
        
    def grad(self, x, y, grad_y):
        """
        Assuming `y=self(x)` and `grad_y=dL/dy' returns pair (grad_x, grad_p), 
        where `grad_x` is a numpy.ndarray equals `grad_x=dL/dx=dL/dy * dy/dx`
        and `grad_p` has the same class as `self` and equals `grad_p=dL/dy * dy/dp`.
        """
        raise NotImplementedError
#         return dx, dp

    def __add__(self, other):
        """Sum of parameters of the networks `self` and `othet`."""
        return NotImplemented
    
    def __sub__(self, other):
        """Difference of parameters of the networks `self` and `othet`."""
        return NotImplemented    
    
    def __mul__(self, other):
        """Multiplication of parameters by a scalar or the inner product."""    
        return NotImplemented
    
    def __matmul__(self, other): 
        """Composition of ANNs. Left associative."""        
        if isinstance(other, ANN):
            return Composition(self, other).normalize()
        return NotImplemented

    @classmethod
    def multilayer(cls, *dims, activation=None, noise=1.):
        if activation is None: activation = Atan
        assert len(dims)>=2
        dims = tuple(reversed(dims))
        net = Linear.random(indim=dims[1], outdim=dims[0], noise=noise)
        for n, m in zip(dims[1:-1], dims[2:]):
            net = net @ activation() @ Linear.random(indim=m, outdim=n)
        return net
    

############################################################################################################

class Composition(ANN):
    """
    Composition of two network `a` and `b` is defined by (a@b)(x) = a(b(x)).
    """
    def __init__(self, a, b):
        assert b.outdim is None or a.indim is None or b.outdim == a.indim
        self.a = a
        self.b = b

    def __repr__(self):
        return f"({self.a}@{self.b})"
        
    def normalize(self):
        if not isinstance(self.b, Composition):
            return self
        return Composition(Composition(self.a, self.b.a).normalize(), self.b.b).normalize()
        
    @property
    def indim(self):
        return self.b.indim if self.b.indim is not None else self.a.indim
    
    @property
    def outdim(self):
        return self.a.outdim if self.a.outdim is not None else self.b.outdim
    
    def __call__(self, x):
        return self.a( self.b ( x ) )
    
    def __add__(self, other):
        if isinstance(other, Composition):
            return Composition(a=self.a+other.a, b=self.b+other.b)
        return NotImplemented
    
    def __sub__(self, other):
        if isinstance(other, Composition):
            return Composition(a=self.a-other.a, b=self.b-other.b)
        return NotImplemented    
    
    def __mul__(self, other):
        if isinstance(other, float): 
            return Composition(a=self.a*other, b=self.b*other)
        if isinstance(other, Composition):
            return self.a*other.a+self.b*other.b
        return NotImplemented
    
    def sum_samples(self):
        return Composition(a=self.a.sum_samples(), b=self.b.sum_samples())

############################################################################################################

class Linear(ANN):
    """
    Linear layer in ANN acting as `y=x*a+b` where `a` is a matrix and `b` is a vector.
    Both `a` and `b` are stored as elements of parameters matrix `p` as follows: p[0]=b, p[1:]=a.
    """
    
    def __init__(self, matrix):
        self.matrix = np.asarray(matrix)
        assert self.matrix.ndim == 2
    
    def __repr__(self):
        return f"{self.outdim}x{self.indim}"
    
    @classmethod
    def random(cls, indim:int, outdim:int, noise=1.):
        matrix = np.random.randn(indim+1,outdim)*noise
        matrix[np.diag_indices(min(indim,outdim))] += 1
        return Linear(matrix)
    
    @property
    def indim(self):
        return self.matrix.shape[-2]-1
    
    @property
    def outdim(self):
        return self.matrix.shape[-1]    
    
    def __call__(self, data):
        assert self.matrix.ndim == 2
        assert data.ndim==2 and data.shape[1] == self.indim, f"Incorrect input shape {data.shape} for matrix {self.matrix.shape}"
        return data @ self.matrix[...,1:,:] + self.matrix[...,0,:]
    
    def grad(self, x, y, grad_y):
        """
        y = x * a + b,
        dy/dx = a,
        grad_x[i] = sum_k dL/dy[k] * dy[k]/dx[i] = (a * grad_y)[i],
        grad_a[i,j] = sum_k dL/dy[k] * dy[k]/da[i,j] = grad_y[i]*x[j],
        grad_b[i] = sum_k dL/dy[k] * dy[k]/db[i] = grad_y[i].
        """
        assert x.ndim == 2
        assert x.shape[-1] == self.indim
        assert y.shape[-1] == self.outdim
        assert x.shape[:-1] == y.shape[:-1]
        assert grad_y.shape == y.shape
        assert self.matrix.ndim == 2
        grad_x = grad_y @ self.matrix[1:].T
        grad_b = np.sum(grad_y[:,None,:], axis=0)
        grad_a = np.sum(grad_y[:,None,:]*x[:,:,None], axis=0)
        grad_p = np.concatenate((grad_b, grad_a), axis=-2)
        return grad_x, Linear(grad_p)
    
    def __add__(self, other):
        if isinstance(other, Linear):
            return Linear(self.matrix + other.matrix)
        return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Linear):
            return Linear(self.matrix - other.matrix)
        return NotImplemented

    def __mul__(self, other):
        if isinstance(other, float): 
            return Linear(self.matrix*other)
        if isinstance(other, Linear):
            return np.sum(self.matrix*other.matrix)
        return NotImplemented

############################################################################################################
            
class Atan(ANN):
    """The netword (layer) applies a scalar function to all coordinates of the input."""
    def __init__(self):
        pass
    
    @property
    def indim(self):
        return None
    
    @property
    def outdim(self):
        return None
    
    def __call__(self, data):
        return np.arctan(data)
    
    def grad(self, x, y, grad_y):
        """
        y = atan(x),
        dL/dy * dy/dx = dL/dx,
        grad_y / (1+x^2) = grad_x.
        """
        assert x.shape == y.shape
        assert y.shape == grad_y.shape
        grad_x = grad_y / (1+x**2)
        return grad_x, self
    
    def __add__(self, other):
        if isinstance(other, Atan):
            return self
        return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Atan):
            return self
        return NotImplemented

    def __mul__(self, other):
        if isinstance(other, float): 
            return self
        if isinstance(other, Atan):
            return 0
        return NotImplemented

############################################################################################################

class Id(ANN):
    """Identity transform as a layer in ANN."""
    def __init__(self):
        pass
    
    @property
    def indim(self):
        return None
    
    @property
    def outdim(self):
        return None
    
    def __call__(self, data):
        return data
    
    def grad(self, x, y, grad_y):
        return grad_y, self
    
    def __add__(self, other):
        if isinstance(other, Id):
            return self
        return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Id):
            return self
        return NotImplemented
    
    def __mul__(self, other):
        if isinstance(other, float): 
            return self
        if isinstance(other, Id):
            return 0
        return NotImplemented


############################################################################################################
# Optimization methods / training
############################################################################################################

# We need a good optimization algorithm to train our network.
def steepest_descent(net, lossfun, x, maxiter=1000, tol=1e-4, verbosity=1, step=1e-2):
    for it in range(maxiter):
        l0, grad_x, grad_net = lossfun.propagate(net=net, x=x)
        norm = np.sqrt(grad_net*grad_net)
        if verbosity>=2 or (verbosity>=1 and it % 100 == 0):
            print(f"{it}: loss {l0} grad {norm}")
        if norm<tol: break
        net = net - grad_net*step
    return net    

############################################################################################################

# See https://en.wikipedia.org/wiki/Limited-memory_BFGS
def lbfgs(net, lossfun, x, maxiter=1000, tol=1e-4, verbosity=1, memory=10, step=1e-2, c1=1e-4, c2=0.9, maxiter2=10):
    def grad(net):
        l0, grad_x, grad_net = lossfun.propagate(net=net, x=x)
        return l0, grad_net        
        
    # Initialization of L-BFGS memory.
    rho, s, y = [], [], []
    xk = net
    fk, gk = grad(xk)    
    
    # Iterate until convergence.
    for it in range(maxiter):
        # Apply the network and compute gradient of loss
        norm = np.sqrt(gk*gk)
        if verbosity>=2 or (verbosity>=1 and it % 100 == 0):
            print(f"{it}: loss {fk} grad {norm}")
        if norm<tol: break        
        # L-BFGS
        q = gk
        alpha = []
        for rhoi, si, yi in zip(rho, s, y):
            alphai = (si*q)*rhoi
            alpha.append(alphai)
            q = q - yi*alphai
        gammak = (s[-1]*y[-1])/(y[-1]*y[-1]) if len(s)>0 else step
        z = q * gammak
        for rhoi, si, yi, alphai in reversed(list(zip(rho, s, y, alpha))):
            betai = rhoi * (z*yi)
            z = z + si*(alphai-betai)
        proj = z*gk 
        if proj>0: 
            z = z*(-1.)
            proj = -proj
        # Line search along direction -z.
        # Searching for an interval, containg a minimum
        mu_right = 1. 
        for it2 in range(100):
            xk_right = xk + z*mu_right
            fk_right, gk_right = grad(xk_right)
            proj_right = z*gk_right
            if proj_right>0: break
            mu_right = mu_right*1.5
        mu_left = 0.
        fk_left = fk
        gk_left = gk
        proj_left = proj
        # Minimum is somewhere between mu_left and mu_right. 
        # Using Brent method for search of the minimum.
        # https://en.wikipedia.org/wiki/Brent%27s_method
        for it3 in range(maxiter2):
            assert proj_left<=0 and proj_right>=0
            if it2 % 2 == 0:
                # In out approximation g(mu)=[proj_right*(mu-mu_left)-proj_left*(mu-mu_right)]/(mu_right-mu_left).
                # g = [mu*(proj_right-proj_left) + proj_left*mu_right-proj_right*mu_left]/(mu_right-mu_left).
                # Minimum of g(mu) is at:
                mu_new = (proj_right*mu_left-proj_left*mu_right)/(proj_right-proj_left)
            else:
                # Middle point of the search interval.
                mu_new = (mu_left+mu_right)/2
            assert mu_left<=mu_new<=mu_right                
            # Try step size mu_new.
            sk = z*mu_new 
            xk_new = xk + sk
            # Compute new gradient
            fk_new, gk_new = grad(xk_new)

            # Check Wolfe condition
            # https://en.wikipedia.org/wiki/Wolfe_conditions
            cond1 = fk_new <= fk + c1*mu_new*proj
            proj_new = z*gk_new
            cond2 = proj_new >= c2*proj # Weak
            # cond2 = abs(proj_new) <= abs(c2*proj) # Strong
            if cond1 and cond2: break
                
            # Update search interval.
            if proj_new>0:
                mu_right, fk_right, gk_right, proj_right = mu_new, fk_new, gk_new, proj_new
            else:
                mu_left, fk_left, gk_left, proj_left = mu_new, fk_new, gk_new, proj_new
                    
        # Add new information to the memory.
        yk = gk_new - gk
        rhok = 1. / (yk*gk)
        s.insert(0, sk)
        y.insert(0, yk)
        rho.insert(0, rhok)

        # Update the solution approximation.
        fk, xk, gk = fk_new, xk_new, gk_new
        
        # Remove unnecessary samples from memory.
        s = s[:memory]
        y = y[:memory]
        rho = rho[:memory]
        
    return xk

